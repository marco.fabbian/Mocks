FROM maven:3.6.3-jdk-11-slim

WORKDIR /usr/local/mock

ARG JAR_FILE=build/libs/mocks-0.0.1-SNAPSHOT-plain.jar
COPY ${JAR_FILE} ./mocks-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["sh", "-c", "java -jar mocks-0.0.1-SNAPSHOT.jar"]