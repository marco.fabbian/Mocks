# CoreBank

## Table of Contents
```
I. Overview

II. Entities

III. Compilation & Run


```

## I. Overview
This application is a big mock that produce data for Apache Kafka message broker.
The attempt here is to demonstrate the event sourcing architecture using Kafka.

## II. Entities
This app produce avro schema messages for the following entities : 
1. Transaction
2. Account
3. Isin
4. Taxation profile

## III. Compilation & Run
In order to compile this project it is necessary to run the "build" on the Gradle project.

The avro schema build is for now manual and this is the command to build the avro files :
```
java -jar C:\Users\marcofabbian\source\repos\avrolib\avro-tools-1.11.1.jar compile schema C:\Users\marcofabbian\source\repos\mock\src\main\kotlin\com\marcofabbian\transaction\data\*.avsc C:\Users\marcofabbian\source\repos\mock\src\main\java\
```

The application can be run with the class above, that contains the main function
```
com.marcofabbian.app.ProducerApp
```