package com.marcofabbian.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@SpringBootApplication(scanBasePackages = arrayOf("com.marcofabbian.transaction.*"))
@EnableJpaRepositories(basePackages = arrayOf("com.marcofabbian.transaction.*"))
@EnableTransactionManagement
@ComponentScan(basePackages = arrayOf("com.marcofabbian.transaction*"))
@EntityScan("com.marcofabbian.transaction.data")
class ProducerApp

fun main(args: Array<String>) {
	runApplication<ProducerApp>(*args)
}