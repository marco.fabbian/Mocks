package com.marcofabbian.transaction.mock

import com.marcofabbian.transaction.builder.*
import com.marcofabbian.transaction.client.ProducerClient
import com.marcofabbian.transaction.config.*
import com.marcofabbian.transaction.data.AccountType
import com.marcofabbian.transaction.jpa.CustomerAccountService
import com.marcofabbian.transaction.jpa.CustomerInformationService
import com.marcofabbian.transaction.jpa.InstrumentService
import com.marcofabbian.transaction.jpa.TaxationService
import org.apache.kafka.clients.admin.NewTopic
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.kafka.config.TopicBuilder
import org.springframework.stereotype.Service
import java.lang.Exception
import java.util.UUID

@Service
class TransactionService(
    private val accountService: CustomerAccountService,
    private val customerInformationService: CustomerInformationService,
    private val instrumentService: InstrumentService,
    private val taxationService: TaxationService,
    private val transactionClient: ProducerClient,
    private val transactionTopic: TransactionTopic,

    private val accountTopic: AccountTopic,
    private val isinTopic: IsinTopic,
    private val taxationTopic:TaxationTopic,
    private val customerTopic: CustomerTopic
    ) {
    private val logger = LoggerFactory.getLogger(TransactionService::class.java)

    @Bean
    fun transactionTopicSetup(): NewTopic {
        return with(transactionTopic){
            TopicBuilder
                .name(topic)
                .partitions(partitions.toInt())
                .replicas(replications.toInt())
                .build()
        }
    }

    @Bean
    fun isinTopicSetup(): NewTopic {
        return with(isinTopic){
            TopicBuilder
                .name(topic)
                .partitions(partitions.toInt())
                .replicas(replications.toInt())
                .build()
        }
    }

    @Bean
    fun accountTopicSetup(): NewTopic {
        return with(accountTopic){
            TopicBuilder
                .name(topic)
                .partitions(partitions.toInt())
                .replicas(replications.toInt())
                .build()
        }
    }

    @Bean
    fun taxationTopicSetup(): NewTopic {
        return with(taxationTopic) {
            TopicBuilder
                .name(topic)
                .partitions(partitions.toInt())
                .replicas(replications.toInt())
                .build()
        }
    }

    @Bean
    fun clientTopicSetup(): NewTopic {
        return with(customerTopic) {
            TopicBuilder
                .name(topic)
                .partitions(partitions.toInt())
                .replicas(replications.toInt())
                .build()
        }
    }

    fun setupDataMock(numberOfTransaction:Int):Boolean {
        logger.info(::TransactionService.javaClass.name + " :: send()")
        (1..numberOfTransaction).map {
            try {

            val id = UUID.randomUUID().toString()
            val clientid = UUID.randomUUID()

            val customer = CustomerBuilder().build(clientid)
            val instrument = InstrumentBuilder().build()
            val accountCash = AccountBuilder().build(AccountType.CASH, clientid)
            val accountSec = AccountBuilder().build(AccountType.CUSTODY, clientid)
            val taxationProfile = TaxationBuilder().build()

            //TODO : Refactor this code and move to an independent class.
            customerInformationService.insert(customer)
            instrumentService.insert(instrument)
            accountService.insert(accountCash)
            accountService.insert(accountSec)
            taxationService.insert(taxationProfile)

            val transaction = TransactionBuilder().build(instrument.isin.toString(), taxationProfile.id.toString(), mutableListOf(accountCash, accountSec, accountSec))
            transactionClient.send(transactionTopic.topic, id, transaction)
            } catch (e:Exception) {
                return false
            }
        }
        return true
    }
}