package com.marcofabbian.transaction.client

import com.marcofabbian.transaction.config.KafkaCluster
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerializer
import org.apache.avro.specific.SpecificRecordBase
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.serialization.StringSerializer
import java.time.Duration
import java.util.*

class ConsumerClient(private val klusterConfig: KafkaCluster) {

    fun <V: SpecificRecordBase>  read(topic:String):List<Pair<String,V>> {
        val timeout = Duration.ofSeconds(60)
        val props = Properties()
        props["bootstrap.servers"] = klusterConfig.bootstrapServers
        props["key.serializer"] = StringSerializer::class.java
        props["value.serializer"] = SpecificAvroSerializer::class.java
        props["schema.registry.url"] = klusterConfig.schemaRegistry

        val consumer = KafkaConsumer<String, V>(props)
        consumer.subscribe(Collections.singletonList(topic))
        val records:ConsumerRecords<String, V> = consumer.poll(timeout)

        var list = mutableListOf<Pair<String,V>>()

        records.forEach {
            val key = it.key()
            val value = it.value()
            list.add(Pair<String,V>(key, value))
        }

        return list
    }
}