package com.marcofabbian.transaction.client

import com.marcofabbian.transaction.config.KafkaCluster
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerializer
import org.apache.avro.specific.SpecificRecordBase
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import org.slf4j.LoggerFactory
import org.springframework.kafka.support.SendResult
import org.springframework.stereotype.Component
import java.util.*

@Component
class ProducerClient(private val klusterConfig: KafkaCluster) {

    private val logger = LoggerFactory.getLogger(ProducerClient::class.java)

    fun <V: SpecificRecordBase> send(topic:String, key:String, value:V):SendResult<String,V> {
        val props = Properties()
        props["bootstrap.servers"] = klusterConfig.bootstrapServers
        props["key.serializer"] = StringSerializer::class.java
        props["value.serializer"] = SpecificAvroSerializer::class.java
        props["schema.registry.url"] = klusterConfig.schemaRegistry
        props["partition.assignment.strategy"] = "org.apache.kafka.clients.consumer.RoundRobinAssignor"

        val producer = KafkaProducer<String, V>(props)
        val record = ProducerRecord(topic, key, value)
        val result = producer.send(record).get()
        return SendResult<String, V>(record, result)
    }
}