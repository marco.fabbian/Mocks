package com.marcofabbian.transaction.client

import com.marcofabbian.transaction.data.EnrichedTransaction
import com.marcofabbian.transaction.config.KafkaCluster
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde
import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.kstream.*
import org.springframework.kafka.support.serializer.JsonSerde
import org.springframework.stereotype.Component
import java.time.Duration
import java.util.*

@Component
class StreamClient(private val klusterConfig: KafkaCluster) {

    fun <L:SpecificRecord, R:SpecificRecord, O> stream(leftJoin:String, rightJoin:String, resultJoin:String, joiner:ValueJoiner<L,R,O> ): KafkaStreams {
        val streamConf = mapOf( "schema.registry.url" to klusterConfig.schemaRegistry,
            StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG to Serdes.UUID()::class.java,
            StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG to SpecificAvroSerde::class.java)

        val leftSerdes = SpecificAvroSerde<L>();
        leftSerdes.configure(streamConf, false)

        val rightSerdes = SpecificAvroSerde<R>()
        rightSerdes.configure(streamConf, false)

        val enrichSerdes = JsonSerde<EnrichedTransaction>()
        enrichSerdes.configure(streamConf, false)

        var builder = StreamsBuilder()
        val strTransaction = builder
            .stream(leftJoin, Consumed.with(Serdes.String(), leftSerdes))

        val strIsin = builder
            .stream(rightJoin, Consumed.with(Serdes.String(), rightSerdes))

        strTransaction.join(strIsin, joiner,  JoinWindows.of(Duration.ofHours(10)), StreamJoined.with(Serdes.String(), leftSerdes, rightSerdes))
            .mapValues { _, o -> o.toString() }
            .peek { _, jsonValue -> jsonValue }
            .to(resultJoin, Produced.with(Serdes.String(), Serdes.String()))

        val topology = builder.build()

        val props = Properties()
        props["bootstrap.servers"] = klusterConfig.bootstrapServers
        props["application.id"] = "EnrichedTransaction-Stream"
        val streams = KafkaStreams(topology, props)
        streams.start()

        return streams
    }

}