package com.marcofabbian.transaction.controller

import com.fasterxml.jackson.annotation.JsonIncludeProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.marcofabbian.transaction.mock.TransactionService
import org.springframework.web.bind.annotation.*

@RestController
class MockDataController(
    val service: TransactionService
) {
    @GetMapping("/about")
    fun about() : String {
        return "Mock Data rest api >>> About function"
    }

    @PostMapping("/datamock")
    @ResponseBody
    fun dataMock(@RequestParam numberOfTransaction: Int): Boolean {
        return service.setupDataMock(numberOfTransaction)
    }
}