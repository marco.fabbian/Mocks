package com.marcofabbian.transaction.builder


import com.marcofabbian.transaction.data.AccountType
import com.marcofabbian.transaction.data.Currency
import com.marcofabbian.transaction.data.CustomerAccount
import java.util.UUID

class AccountBuilder : BuilderBase() {

    fun build(type:AccountType? = null, customerId:UUID): CustomerAccount {
        val rnd = createRnd(0, currencies.count()-1)
        val owner = combineNameAndFamilyName()

        return CustomerAccount().apply {
            this.id=super.createGuId()
            this.currency=Currency.valueOf(currencies[rnd].first)
            this.type=getAccountType()
            this.contractId=customerId
        }
    }

    private fun getAccountType(): AccountType {
        return (when (accountTypes[createRnd(0, accountTypes.count()-1)]) {
            AccountType.CASH.toString() -> AccountType.CASH
            AccountType.CUSTODY.toString() -> AccountType.CUSTODY
            else -> AccountType.CASH
        })
    }

    private val accountTypes:List<String> = listOf("CASH", "CUSTODY")

}