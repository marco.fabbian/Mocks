package com.marcofabbian.transaction.builder


import com.marcofabbian.transaction.avroschema.*
import com.marcofabbian.transaction.data.AccountType
import com.marcofabbian.transaction.data.CustomerAccount

class TransactionBuilder : BuilderBase() {
        fun build(isin:String, taxId:String, bankAccounts:MutableList<CustomerAccount>? = null): Transaction {
            val transaction = Transaction.newBuilder().apply {
                id = createId()
                messageCreation = createDateTime()
                price = createPrice()
                currency = createCurrency()
                quantity = createInt(1, 5)
                instrumentID = isin
                bookingDateTime = createDateTime(1)
                settlementDateTime = createDateTime(3)
                bookingCenterID = "IT"
                taxationId = taxId
            }
            if(bankAccounts != null)
                transaction.accounts = getAccounts(bankAccounts)

            return transaction.build()
        }

    private fun getAccounts(bankAccounts:List<CustomerAccount>):List<Account>{
        var accounts:MutableList<Account> = mutableListOf()
        bankAccounts.forEachIndexed { index, account ->
            accounts.add(Account().apply {
                actor = if(index.isEven())
                    Actor.BUYER
                else
                    Actor.SELLER

                accountId = account.id.toString()

                accountType = if(account.type == AccountType.CASH)
                    AccountType.CASH.toString()
                else
                    AccountType.CUSTODY.toString()

                accountCurrency = if(account.type == AccountType.CASH)
                    account.currency.name
                 else
                    ""
                accountUsage = com.marcofabbian.transaction.avroschema.AccountUsage.DEBIT
            })
        }
        return accounts
    }
}