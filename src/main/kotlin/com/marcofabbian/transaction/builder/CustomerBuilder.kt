package com.marcofabbian.transaction.builder

import com.marcofabbian.transaction.data.Address
import com.marcofabbian.transaction.data.CustomerInformation
import java.util.*

class CustomerBuilder : BuilderBase() {

    fun build(customerId: UUID): CustomerInformation {
        var owner = combineNameAndFamilyName()
        var cty = createCity()
        var prv = createProvince(cty)
        var ctry = createCountryCode(cty)
        var adrs = Address().apply {
            id = super.createGuId()
            street = createAddress()
            city = cty
            province = prv
            country = ctry.desc
        }
        return CustomerInformation().apply {
            id = super.createGuId()
            name = owner.first
            surname = owner.second
            address = adrs
            contractId = customerId
        }
    }
}