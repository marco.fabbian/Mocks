package com.marcofabbian.transaction.builder

import com.marcofabbian.transaction.data.Instrument

class InstrumentBuilder : BuilderBase() {


    fun build(): Instrument {
        val rnd = createRnd(0, isinList.count()-1)

        return Instrument().apply {
                id = createGuId()
                isin = isinList[rnd].second
                name = isinList[rnd].third
                wkn = detailList[rnd].first
                market = detailList[rnd].second
                symbol = detailList[rnd].third
            }
    }
}