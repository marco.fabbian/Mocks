package com.marcofabbian.transaction.builder

import com.marcofabbian.transaction.data.Taxation

class TaxationBuilder  : BuilderBase() {
    fun build(): Taxation {
        var countryCode = createCountryCode()
        return  Taxation().apply {
            id = createGuId()
            this.countryCode = countryCode.Alpha_2
            taxProfile = createInt(1,20).toBigDecimal()
            taxCode = createTaxCode(countryCode.Alpha_2)
        }
    }
}