package com.marcofabbian.transaction.builder

import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*

abstract class BuilderBase {
    fun createPrice(): BigDecimal {
        val int = createRnd(1, 100)
        val dec = createRnd(1, 99)
        return BigDecimal("$int.$dec")
    }

    fun createDateTime(days: Long = 0): Long {
        val now = LocalDateTime
            .now()
            .plusDays(days)
            .toInstant(ZoneOffset.UTC)
        return Date.from(now).time
    }

    fun createInt(min: Int = 1, max: Int = 10): Int = (Random().nextInt(max - min + 1) + min)
    fun createRnd(min: Int, max: Int): Int = (Random().nextInt(max - min + 1) + min)
    fun createGuId(): UUID = UUID.randomUUID()
    fun createId(): String = UUID.randomUUID().toString()
    fun Int.isEven(): Boolean = this % 2 == 0
    fun createTaxCode(countryCode:String):String = countryCode+"_"+createId()
    fun createCurrency(): String = currencies[createRnd(0, currencies.count()-1)].toString()
    val currencies:List<Pair<String,String>> =  listOf(Pair("EUR", "Euro"), Pair("CHF", "Swiss Frank"), Pair("USD", "US Dollar"), Pair("CAN", "Canadian Dollar"), Pair("GBP", "British Pound"))
    fun createAddress():String = addresses[createRnd(0, addresses.count()-1)] + " " + streets[createRnd(0, streets.count()-1)]
    val addresses:List<String> = listOf("first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth", "twenty-first", "twenty-second", "twenty-third", "twenty-fourth", "twenty-fifth", "twenty-sixth", "twenty-seventh", "twenty-eighth", "twenty-ninth", "thirtieth", "thirty-first")
    val streets:List<String> = listOf("road", "avenue", "drive", "terrace", "parade", "side street", "side road", "lane", "alley", "boulevard", "highway", "strip")
    fun createCity():String = cities[createRnd(0, cities.count()-1)]
    val cities:List<String> = listOf("Tokyo","Delhi","Shanghai","São Paulo","Mexico City","Cairo","Mumbai","Beijing","Dhaka","Osaka","New York","Karachi","Buenos Aires","Chongqing","Istanbul","Kolkata","Manila","Lagos","Rio de Janeiro","Tianjin","Kinshasa","Guangzhou","Los Angeles","Moscow","Shenzhen","Lahore","Bangalore","Paris","Bogotá","Jakarta","Chennai","Lima","Bangkok","Seoul","Nagoya","Hyderabad","London","Tehran","Chicago","Chengdu","Nanjing","Wuhan","Ho Chi Minh City","Luanda","Ahmedabad","Kuala Lumpur","Xi'an","Hong Kong","Dongguan","Hangzhou","Foshan","Shenyang","Riyadh","Baghdad","Santiago","Surat","Madrid","Suzhou","Pune","Harbin","Houston","Dallas","Toronto","Dar es Salaam","Miami","Belo Horizonte","Singapore","Philadelphia","Atlanta","Fukuoka","Khartoum","Barcelona","Johannesburg","Saint Petersburg","Qingdao","Dalian","Washington","Yangon","Alexandria","Jinan","Guadalajara")
    fun createProvince(city:String):String = city
    fun combineNameAndFamilyName():Pair<String, String> = Pair(names[createRnd(0, names.count()-1)] , surnames[createRnd(0, surnames.count()-1)])
    private val names:List<String> = listOf("James", "Robert", "John", "Michael", "David", "William", "Richard", "Joseph", "Thomas", "Charles", "Christopher", "Daniel", "Matthew", "Anthony", "Mark", "Donald", "Steven", "Paul", "Andrew", "Joshua")
    private val surnames:List<String> = listOf("Smith", "Jones", "Williams","Taylor","Brown","Davies","Evans","Thomas","Wilson","Johnson","Roberts","Robinson","Thompson","Wright","Walker","White","Edwards","Hughes","Green","Hall")


    fun createCountryCode(city:String? = null):CountryCode {
        var country = when (city) {
                "Tokyo"-> CountryCodes.find { it.desc == "Japan" }
                "Delhi"-> CountryCodes.find { it.desc == "India" }
                "Shanghai"-> CountryCodes.find { it.desc == "China" }
                "São Paulo"-> CountryCodes.find { it.desc == "Brazil" }
                "Mexico City"-> CountryCodes.find { it.desc == "Mexico" }
                "Cairo"-> CountryCodes.find { it.desc == "Egypt" }
                "Mumbai"-> CountryCodes.find { it.desc == "India" }
                "Beijing"-> CountryCodes.find { it.desc == "China" }
                "Dhaka"-> CountryCodes.find { it.desc == "Bangladesh" }
                "Osaka"-> CountryCodes.find { it.desc == "Japan" }
                "New York"-> CountryCodes.find { it.desc == "United States of America" }
                "Karachi"-> CountryCodes.find { it.desc == "Pakistan " }
                "Buenos Aires"-> CountryCodes.find { it.desc == "Argentina" }
                "Chongqing"-> CountryCodes.find { it.desc == "China " }
                "Istanbul"-> CountryCodes.find { it.desc == "Türkiye" }
                "Kolkata"-> CountryCodes.find { it.desc == "India" }
                "Manila"-> CountryCodes.find { it.desc == "Philipinnes" }
                "Lagos"-> CountryCodes.find { it.desc == "Nigeria " }
                "Rio de Janeiro"-> CountryCodes.find { it.desc == "Brazil" }
                "Tianjin"-> CountryCodes.find { it.desc == "China" }
                "Kinshasa"-> CountryCodes.find { it.desc == "Democratic Republic of the Congo" }
                "Guangzhou"-> CountryCodes.find { it.desc == "China " }
                "Los Angeles"-> CountryCodes.find { it.desc == "United States of America" }
                "Moscow"-> CountryCodes.find { it.desc == "Russian Federation" }
                "Shenzhen"-> CountryCodes.find { it.desc == "China" }
                "Lahore"-> CountryCodes.find { it.desc == "Pakistan" }
                "Bangalore"-> CountryCodes.find { it.desc == "India" }
                "Paris"-> CountryCodes.find { it.desc == "France" }
                "Bogotá"-> CountryCodes.find { it.desc == "Argentina" }
                "Jakarta"-> CountryCodes.find { it.desc == "Indonesia" }
                "Chennai"-> CountryCodes.find { it.desc == "India" }
                "Lima"-> CountryCodes.find { it.desc == "Chile" }
                "Bangkok"-> CountryCodes.find { it.desc == "Japan" }
                "Seoul"-> CountryCodes.find { it.desc == "Republic of Korea" }
                "Nagoya"-> CountryCodes.find { it.desc == "Japan" }
                "Hyderabad"-> CountryCodes.find { it.desc == "India" }
                "London"-> CountryCodes.find { it.desc == "United Kingdom of Great Britain and Northern Ireland" }
                "Tehran"-> CountryCodes.find { it.desc == "Islamic Republic of Iran" }
                "Chicago"-> CountryCodes.find { it.desc == "United States of America" }
                "Chengdu"-> CountryCodes.find { it.desc == "India" }
                "Nanjing"-> CountryCodes.find { it.desc == "Pakistan" }
                "Wuhan"-> CountryCodes.find { it.desc == "China" }
                "Ho Chi Minh City"-> CountryCodes.find { it.desc == "Viet Nam" }
                "Luanda"-> CountryCodes.find { it.desc == "Angola" }
                "Ahmedabad"-> CountryCodes.find { it.desc == "India" }
                "Kuala Lumpur"-> CountryCodes.find { it.desc == "Malaysia" }
                "Xi'an"-> CountryCodes.find { it.desc == "China" }
                "Hong Kong"-> CountryCodes.find { it.desc == "Hong Kong" }
                "Dongguan"-> CountryCodes.find { it.desc == "China" }
                "Hangzhou"-> CountryCodes.find { it.desc == "Japan" }
                "Foshan"-> CountryCodes.find { it.desc == "China" }
                "Shenyang"-> CountryCodes.find { it.desc == "China" }
                "Riyadh"-> CountryCodes.find { it.desc == "Saudi Arabia" }
                "Baghdad"-> CountryCodes.find { it.desc == "Iraq" }
                "Santiago"-> CountryCodes.find { it.desc == "Chile" }
                "Surat"-> CountryCodes.find { it.desc == "Japan" }
                "Madrid"-> CountryCodes.find { it.desc == "Spain" }
                "Suzhou"-> CountryCodes.find { it.desc == "Japan" }
                "Pune"-> CountryCodes.find { it.desc == "India" }
                "Harbin"-> CountryCodes.find { it.desc == "China" }
                "Houston"-> CountryCodes.find { it.desc == "United States of America" }
                "Dallas"-> CountryCodes.find { it.desc == "United States of America" }
                "Toronto"-> CountryCodes.find { it.desc == "Canada" }
                "Dar es Salaam"-> CountryCodes.find { it.desc == "Tanzania" }
                "Miami"-> CountryCodes.find { it.desc == "United States of America" }
                "Belo Horizonte"-> CountryCodes.find { it.desc == "Brazil" }
                "Singapore"-> CountryCodes.find { it.desc == "Singapore" }
                "Philadelphia"-> CountryCodes.find { it.desc == "United States of America" }
                "Atlanta"-> CountryCodes.find { it.desc == "United States of America" }
                "Fukuoka"-> CountryCodes.find { it.desc == "Japan" }
                "Khartoum"-> CountryCodes.find { it.desc == "Sudan" }
                "Barcelona"-> CountryCodes.find { it.desc == "Spain" }
                "Johannesburg"-> CountryCodes.find { it.desc == "South Africa" }
                "Saint Petersburg"-> CountryCodes.find { it.desc == "Russian Federation" }
                "Qingdao"-> CountryCodes.find { it.desc == "Japan" }
                "Dalian"-> CountryCodes.find { it.desc == "Japan" }
                "Washington"-> CountryCodes.find { it.desc == "United States of America" }
                "Yangon"-> CountryCodes.find { it.desc == "Japan" }
                "Alexandria"-> CountryCodes.find { it.desc == "Japan" }
                "Jinan"-> CountryCodes.find { it.desc == "Japan" }
                "Guadalajara"-> CountryCodes.find { it.desc == "Japan" }
                else -> CountryCodes[createRnd(0, CountryCodes.count()-1)]
            }

        return country ?: createCountryCode()
    }


    class CountryCode(val desc:String, val Alpha_2:String, val Alpha_3:String, val num:String)
    private val CountryCodes = listOf<CountryCode>(
        CountryCode("Afghanistan","AF","AFG","004"),
        CountryCode("Åland Islands","AX","ALA","248"),
        CountryCode("Albania","AL","ALB","008"),
        CountryCode("Algeria","DZ","DZA","012"),
        CountryCode("American Samoa","AS","ASM","016"),
        CountryCode("Andorra","AD","AND","020"),
        CountryCode("Angola","AO","AGO","024"),
        CountryCode("Anguilla","AI","AIA","660"),
        CountryCode("Antarctica","AQ","ATA","010"),
        CountryCode("Antigua and Barbuda","AG","ATG","028"),
        CountryCode("Argentina","AR","ARG","032"),
        CountryCode("Armenia","AM","ARM","051"),
        CountryCode("Aruba","AW","ABW","533"),
        CountryCode("Australia","AU","AUS","036"),
        CountryCode("Austria","AT","AUT","040"),
        CountryCode("Azerbaijan","AZ","AZE","031"),
        CountryCode("Bahamas","BS","BHS","044"),
        CountryCode("Bahrain","BH","BHR","048"),
        CountryCode("Bangladesh","BD","BGD","050"),
        CountryCode("Barbados","BB","BRB","052"),
        CountryCode("Belarus","BY","BLR","112"),
        CountryCode("Belgium","BE","BEL","056"),
        CountryCode("Belize","BZ","BLZ","084"),
        CountryCode("Benin","BJ","BEN","204"),
        CountryCode("Bermuda","BM","BMU","060"),
        CountryCode("Bhutan","BT","BTN","064"),
        CountryCode("Bolivia (Plurinational State of),","BO","BOL","068"),
        CountryCode("Bonaire, Sint Eustatius and Saba","BQ","BES","535"),
        CountryCode("Bosnia and Herzegovina","BA","BIH","070"),
        CountryCode("Botswana","BW","BWA","072"),
        CountryCode("Bouvet Island","BV","BVT","074"),
        CountryCode("Brazil","BR","BRA","076"),
        CountryCode("British Indian Ocean Territory","IO","IOT","086"),
        CountryCode("Brunei Darussalam","BN","BRN","096"),
        CountryCode("Bulgaria","BG","BGR","100"),
        CountryCode("Burkina Faso","BF","BFA","854"),
        CountryCode("Burundi","BI","BDI","108"),
        CountryCode("Cabo Verde","CV","CPV","132"),
        CountryCode("Cambodia","KH","KHM","116"),
        CountryCode("Cameroon","CM","CMR","120"),
        CountryCode("Canada","CA","CAN","124"),
        CountryCode("Cayman Islands","KY","CYM","136"),
        CountryCode("Central African Republic","CF","CAF","140"),
        CountryCode("Chad","TD","TCD","148"),
        CountryCode("Chile","CL","CHL","152"),
        CountryCode("China","CN","CHN","156"),
        CountryCode("Christmas Island","CX","CXR","162"),
        CountryCode("Cocos (Keeling), Islands","CC","CCK","166"),
        CountryCode("Colombia","CO","COL","170"),
        CountryCode("Comoros","KM","COM","174"),
        CountryCode("Democratic Republic of the Congo","CD","COD","180"),
        CountryCode("Congo","CG","COG","178"),
        CountryCode("Cook Islands","CK","COK","184"),
        CountryCode("Costa Rica","CR","CRI","188"),
        CountryCode("Côte d'Ivoire","CI","CIV","384"),
        CountryCode("Croatia","HR","HRV","191"),
        CountryCode("Cuba","CU","CUB","192"),
        CountryCode("Curaçao","CW","CUW","531"),
        CountryCode("Cyprus","CY","CYP","196"),
        CountryCode("Czechia","CZ","CZE","203"),
        CountryCode("Denmark","DK","DNK","208"),
        CountryCode("Djibouti","DJ","DJI","262"),
        CountryCode("Dominica","DM","DMA","212"),
        CountryCode("Dominican Republic","DO","DOM","214"),
        CountryCode("Ecuador","EC","ECU","218"),
        CountryCode("Egypt","EG","EGY","818"),
        CountryCode("El Salvador","SV","SLV","222"),
        CountryCode("Equatorial Guinea","GQ","GNQ","226"),
        CountryCode("Eritrea","ER","ERI","232"),
        CountryCode("Estonia","EE","EST","233"),
        CountryCode("Eswatini","SZ","SWZ","748"),
        CountryCode("Ethiopia","ET","ETH","231"),
        CountryCode("Falkland Islands [Malvinas]","FK","FLK","238"),
        CountryCode("Faroe Islands","FO","FRO","234"),
        CountryCode("Fiji","FJ","FJI","242"),
        CountryCode("Finland","FI","FIN","246"),
        CountryCode("France","FR","FRA","250"),
        CountryCode("French Guiana","GF","GUF","254"),
        CountryCode("French Polynesia","PF","PYF","258"),
        CountryCode("French Southern Territories","TF","ATF","260"),
        CountryCode("Gabon","GA","GAB","266"),
        CountryCode("Gambia","GM","GMB","270"),
        CountryCode("Georgia","GE","GEO","268"),
        CountryCode("Germany","DE","DEU","276"),
        CountryCode("Ghana","GH","GHA","288"),
        CountryCode("Gibraltar","GI","GIB","292"),
        CountryCode("Greece","GR","GRC","300"),
        CountryCode("Greenland","GL","GRL","304"),
        CountryCode("Grenada","GD","GRD","308"),
        CountryCode("Guadeloupe","GP","GLP","312"),
        CountryCode("Guam","GU","GUM","316"),
        CountryCode("Guatemala","GT","GTM","320"),
        CountryCode("Guernsey","GG","GGY","831"),
        CountryCode("Guinea","GN","GIN","324"),
        CountryCode("Guinea-Bissau","GW","GNB","624"),
        CountryCode("Guyana","GY","GUY","328"),
        CountryCode("Haiti","HT","HTI","332"),
        CountryCode("Heard Island and McDonald Islands","HM","HMD","334"),
        CountryCode("Holy See","VA","VAT","336"),
        CountryCode("Honduras","HN","HND","340"),
        CountryCode("Hong Kong","HK","HKG","344"),
        CountryCode("Hungary","HU","HUN","348"),
        CountryCode("Iceland","IS","ISL","352"),
        CountryCode("India","IN","IND","356"),
        CountryCode("Indonesia","ID","IDN","360"),
        CountryCode("Islamic Republic of Iran","IR","IRN","364"),
        CountryCode("Iraq","IQ","IRQ","368"),
        CountryCode("Ireland","IE","IRL","372"),
        CountryCode("Isle of Man","IM","IMN","833"),
        CountryCode("Israel","IL","ISR","376"),
        CountryCode("Italy","IT","ITA","380"),
        CountryCode("Jamaica","JM","JAM","388"),
        CountryCode("Japan","JP","JPN","392"),
        CountryCode("Jersey","JE","JEY","832"),
        CountryCode("Jordan","JO","JOR","400"),
        CountryCode("Kazakhstan","KZ","KAZ","398"),
        CountryCode("Kenya","KE","KEN","404"),
        CountryCode("Kiribati","KI","KIR","296"),
        CountryCode("Democratic People's Republic of Korea","KP","PRK","408"),
        CountryCode("Republic of Korea","KR","KOR","410"),
        CountryCode("Kuwait","KW","KWT","414"),
        CountryCode("Kyrgyzstan","KG","KGZ","417"),
        CountryCode("Lao People's Democratic Republic","LA","LAO","418"),
        CountryCode("Latvia","LV","LVA","428"),
        CountryCode("Lebanon","LB","LBN","422"),
        CountryCode("Lesotho","LS","LSO","426"),
        CountryCode("Liberia","LR","LBR","430"),
        CountryCode("Libya","LY","LBY","434"),
        CountryCode("Liechtenstein","LI","LIE","438"),
        CountryCode("Lithuania","LT","LTU","440"),
        CountryCode("Luxembourg","LU","LUX","442"),
        CountryCode("Macao","MO","MAC","446"),
        CountryCode("Madagascar","MG","MDG","450"),
        CountryCode("Malawi","MW","MWI","454"),
        CountryCode("Malaysia","MY","MYS","458"),
        CountryCode("Maldives","MV","MDV","462"),
        CountryCode("Mali","ML","MLI","466"),
        CountryCode("Malta","MT","MLT","470"),
        CountryCode("Marshall Islands","MH","MHL","584"),
        CountryCode("Martinique","MQ","MTQ","474"),
        CountryCode("Mauritania","MR","MRT","478"),
        CountryCode("Mauritius","MU","MUS","480"),
        CountryCode("Mayotte","YT","MYT","175"),
        CountryCode("Mexico","MX","MEX","484"),
        CountryCode("Micronesia (Federated States of),","FM","FSM","583"),
        CountryCode("Moldova (the Republic of),","MD","MDA","498"),
        CountryCode("Monaco","MC","MCO","492"),
        CountryCode("Mongolia","MN","MNG","496"),
        CountryCode("Montenegro","ME","MNE","499"),
        CountryCode("Montserrat","MS","MSR","500"),
        CountryCode("Morocco","MA","MAR","504"),
        CountryCode("Mozambique","MZ","MOZ","508"),
        CountryCode("Myanmar","MM","MMR","104"),
        CountryCode("Namibia","NA","NAM","516"),
        CountryCode("Nauru","NR","NRU","520"),
        CountryCode("Nepal","NP","NPL","524"),
        CountryCode("Netherlands","NL","NLD","528"),
        CountryCode("New Caledonia","NC","NCL","540"),
        CountryCode("New Zealand","NZ","NZL","554"),
        CountryCode("Nicaragua","NI","NIC","558"),
        CountryCode("Niger","NE","NER","562"),
        CountryCode("Nigeria","NG","NGA","566"),
        CountryCode("Niue","NU","NIU","570"),
        CountryCode("Norfolk Island","NF","NFK","574"),
        CountryCode("Northern Mariana Islands","MP","MNP","580"),
        CountryCode("Norway","NO","NOR","578"),
        CountryCode("Oman","OM","OMN","512"),
        CountryCode("Pakistan","PK","PAK","586"),
        CountryCode("Palau","PW","PLW","585"),
        CountryCode("Palestine State of","PS","PSE","275"),
        CountryCode("Panama","PA","PAN","591"),
        CountryCode("Papua New Guinea","PG","PNG","598"),
        CountryCode("Paraguay","PY","PRY","600"),
        CountryCode("Peru","PE","PER","604"),
        CountryCode("Philippines","PH","PHL","608"),
        CountryCode("Pitcairn","PN","PCN","612"),
        CountryCode("Poland","PL","POL","616"),
        CountryCode("Portugal","PT","PRT","620"),
        CountryCode("Puerto Rico","PR","PRI","630"),
        CountryCode("Qatar","QA","QAT","634"),
        CountryCode("Republic of North Macedonia","MK","MKD","807"),
        CountryCode("Réunion","RE","REU","638"),
        CountryCode("Romania","RO","ROU","642"),
        CountryCode("Russian Federation","RU","RUS","643"),
        CountryCode("Rwanda","RW","RWA","646"),
        CountryCode("Saint Barthélemy","BL","BLM","652"),
        CountryCode("Saint Helena, Ascension and Tristan da Cunha","SH","SHN","654"),
        CountryCode("Saint Kitts and Nevis","KN","KNA","659"),
        CountryCode("Saint Lucia","LC","LCA","662"),
        CountryCode("Saint Martin (French part),","MF","MAF","663"),
        CountryCode("Saint Pierre and Miquelon","PM","SPM","666"),
        CountryCode("Saint Vincent and the Grenadines","VC","VCT","670"),
        CountryCode("Samoa","WS","WSM","882"),
        CountryCode("San Marino","SM","SMR","674"),
        CountryCode("Sao Tome and Principe","ST","STP","678"),
        CountryCode("Saudi Arabia","SA","SAU","682"),
        CountryCode("Senegal","SN","SEN","686"),
        CountryCode("Serbia","RS","SRB","688"),
        CountryCode("Seychelles","SC","SYC","690"),
        CountryCode("Sierra Leone","SL","SLE","694"),
        CountryCode("Singapore","SG","SGP","702"),
        CountryCode("Sint Maarten (Dutch part),","SX","SXM","534"),
        CountryCode("Slovakia","SK","SVK","703"),
        CountryCode("Slovenia","SI","SVN","705"),
        CountryCode("Solomon Islands","SB","SLB","090"),
        CountryCode("Somalia","SO","SOM","706"),
        CountryCode("South Africa","ZA","ZAF","710"),
        CountryCode("South Georgia and the South Sandwich Islands","GS","SGS","239"),
        CountryCode("South Sudan","SS","SSD","728"),
        CountryCode("Spain","ES","ESP","724"),
        CountryCode("Sri Lanka","LK","LKA","144"),
        CountryCode("Sudan","SD","SDN","729"),
        CountryCode("Suriname","SR","SUR","740"),
        CountryCode("Svalbard and Jan Mayen","SJ","SJM","744"),
        CountryCode("Sweden","SE","SWE","752"),
        CountryCode("Switzerland","CH","CHE","756"),
        CountryCode("Syrian Arab Republic","SY","SYR","760"),
        CountryCode("Taiwan (Province of China),","TW","TWN","158"),
        CountryCode("Tajikistan","TJ","TJK","762"),
        CountryCode("Tanzania, United Republic of","TZ","TZA","834"),
        CountryCode("Thailand","TH","THA","764"),
        CountryCode("Timor-Leste","TL","TLS","626"),
        CountryCode("Togo","TG","TGO","768"),
        CountryCode("Tokelau","TK","TKL","772"),
        CountryCode("Tonga","TO","TON","776"),
        CountryCode("Trinidad and Tobago","TT","TTO","780"),
        CountryCode("Tunisia","TN","TUN","788"),
        CountryCode("Türkiye","TR","TUR","792"),
        CountryCode("Turkmenistan","TM","TKM","795"),
        CountryCode("Turks and Caicos Islands","TC","TCA","796"),
        CountryCode("Tuvalu","TV","TUV","798"),
        CountryCode("Uganda","UG","UGA","800"),
        CountryCode("Ukraine","UA","UKR","804"),
        CountryCode("United Arab Emirates","AE","ARE","784"),
        CountryCode("United Kingdom of Great Britain and Northern Ireland","GB","GBR","826"),
        CountryCode("United States Minor Outlying Islands","UM","UMI","581"),
        CountryCode("United States of America","US","USA","840"),
        CountryCode("Uruguay","UY","URY","858"),
        CountryCode("Uzbekistan","UZ","UZB","860"),
        CountryCode("Vanuatu","VU","VUT","548"),
        CountryCode("Venezuela (Bolivarian Republic of),","VE","VEN","862"),
        CountryCode("Viet Nam","VN","VNM","704"),
        CountryCode("Virgin Islands (British),","VG","VGB","092"),
        CountryCode("Virgin Islands (U.S.),","VI","VIR","850"),
        CountryCode("Wallis and Futuna","WF","WLF","876"),
        CountryCode("Western Sahara","EH","ESH","732"),
        CountryCode("Yemen","YE","YEM","887"),
        CountryCode("Zambia","ZM","ZMB","894"),
        CountryCode("Zimbabwe","ZW","ZWE","716")
        )

    val isinList: List<Triple<String, String, String>> = listOf(
        Triple("eb802976-32d7-11ed-a261-0242ac120002", "US5949181045", "Microsoft Corp."),
        Triple("efe4d0fc-32d7-11ed-a261-0242ac120002", "US38259P5089", "Google Inc."),
        Triple("f34c3bb8-32d7-11ed-a261-0242ac120002", "US0378331005", "Apple Inc."),
        Triple("0aec5820-32d8-11ed-a261-0242ac120002", "NL0000729408", "Bear Zertifikat auf DAX"),
        Triple("0d72e118-32d8-11ed-a261-0242ac120002", "JP3946600008", "Yusen Logistics Co."),
        Triple("0fefe51c-32d8-11ed-a261-0242ac120002", "DE000DZ21632", "Aktienanleihe auf BASF"),
        Triple("12c28d76-32d8-11ed-a261-0242ac120002", "DE000DB7HWY7", "Aktienanleihe Plus auf Al"),
        Triple("1560aa36-32d8-11ed-a261-0242ac120002", "DE000CM7VX13", "Aktienanleihe Plus auf"),
        Triple("18eb1c68-32d8-11ed-a261-0242ac120002", "CH0031240127", "BMW Australia"),
        Triple("1b57373e-32d8-11ed-a261-0242ac120002", "CA9861913023", "Yorbeau Res Inc.")
    )

    val detailList: List<Triple<String, String, String>> = listOf(
        Triple("870747", "Open Market", "MSF"),
        Triple("A0B7FY", "Open Market", "GGQ1"),
        Triple("865985", "Open Market", "APC"),
        Triple("AA0B7X", "Open Market", "DAX"),
        Triple("919744", "Open Market", "YV5"),
        Triple("DZ2163", "Open Market", "WKN"),
        Triple("DB7HWY", "Open Market", "Al"),
        Triple("CM7VX1", "Open Market", "WKA"),
        Triple("A0NWXQ", "Open Market", "AKN"),
        Triple("872300", "Open Market", "UAN")
    )

}