package com.marcofabbian.transaction.data

import java.math.BigDecimal
import java.time.LocalDateTime

data class DomainTransaction(
    val id: String,
    val messageCreation: Long,
    val price: BigDecimal,
    val currency: String,
    val quantity:Int,
    val instrumentID: String,
    val bookingDateTime: Long,
    val settlementDateTime: Long,
    val bookingCenterID: String
){

}