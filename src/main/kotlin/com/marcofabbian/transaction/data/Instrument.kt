package com.marcofabbian.transaction.data

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity(name = "isin")
@Table(name = "isin", schema = "financialinstrument")
class Instrument {
    @Id
    lateinit var id: UUID

    @Column(name = "isin")
    lateinit var isin: String

    @Column(name = "name")
    lateinit var name:String

    @Column(name = "wkn")
    lateinit var wkn:String

    @Column(name = "market")
    lateinit var market:String

    @Column(name = "symbol")
    lateinit var symbol:String
}