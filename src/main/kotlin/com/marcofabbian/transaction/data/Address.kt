package com.marcofabbian.transaction.data

import java.util.UUID
import javax.persistence.*

@Entity(name = "address")
@Table(name = "address", schema = "customerinformation")
class Address {
    @Id
    lateinit var id: UUID

    @Column(name = "street")
    lateinit var street:String

    @Column(name = "city")
    lateinit var city:String

    @Column(name = "province")
    lateinit var province:String

    @Column(name = "country")
    lateinit var country:String

    @OneToOne(mappedBy = "address")
    lateinit var customerInformation:CustomerInformation
}
