package com.marcofabbian.transaction.data

import java.math.BigDecimal
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity(name = "taxation")
@Table(name = "taxation", schema = "taxationservice")
class Taxation {
    @Id
    lateinit var id: UUID

    @Column(name = "countrycode")
    lateinit var countryCode: String

    @Column(name = "taxprofile")
    lateinit var taxProfile: BigDecimal

    @Column(name = "taxcode")
    lateinit var taxCode: String
}