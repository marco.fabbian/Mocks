package com.marcofabbian.transaction.data

import java.util.UUID
import javax.persistence.*

@Entity(name="account")
@Table(name="account", schema="customeraccount")
class CustomerAccount {

    @Id
    lateinit var id:UUID

    @Column(name = "Currency")
    @Enumerated(EnumType.STRING)
    lateinit var currency:Currency

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    lateinit var type:AccountType

    @Column(name = "contractid")
    lateinit var contractId:UUID
}


enum class Currency{
    EUR, CHF, CAN, USD, GBP
}

enum class AccountType{
    CASH, CUSTODY, CRYPTO
}
