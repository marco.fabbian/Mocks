package com.marcofabbian.transaction.data

import java.util.*
import javax.persistence.*

@Entity(name = "customer")
@Table(name = "customer", schema = "customerinformation")
class CustomerInformation {
    @Id
    lateinit var id: UUID

    @Column(name = "contractid")
    lateinit var contractId: UUID

    @Column(name = "name")
    lateinit var name:String

    @Column(name = "surname")
    lateinit var surname:String

    @OneToOne(cascade = arrayOf(CascadeType.ALL))
    @JoinColumn(name = "addressid", referencedColumnName = "id")
    lateinit var address: Address
}