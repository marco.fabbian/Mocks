package com.marcofabbian.transaction.data

import com.marcofabbian.transaction.avroschema.Isin
import com.marcofabbian.transaction.avroschema.Transaction
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDateTime
import java.util.*

interface ISerializer {
        fun toJson():String
}
data class EnrichedTransaction(
        private val id:String,
        private val messageCreation:LocalDateTime,
        private val price:BigDecimal,
        private val currency:String,
        private val quantity:Int,
        private val instrumentID:String,
        private val bookingDateTime:LocalDateTime,
        private val settlementDateTime:LocalDateTime,
        private val bookingCenterID:String,
        private val isin:String,
        private val name:String,
        private val wkn:String,
        private val symbol:String,
        private val market:String
) {
        override fun toString():String {
                return """
                        {
                                "id" : "$id",
                                "messageCreation" : "$messageCreation"
                                "price" : "$price"
                                "currency" : "$currency"
                                "quantity" : "$quantity"
                                "instrumentID" : "$instrumentID" 
                                "bookingDateTime" : "$bookingDateTime" 
                                "settlementDateTime" : "$settlementDateTime"
                                "bookingCenterID" : "$bookingCenterID"
                                "isin" : "$isin"
                                "name" : "$name"
                                "wkn" : "$wkn"
                                "symbol" : "$symbol"
                                "market" : "$market"
                        }
                        """
        }

        companion object {
                fun mapper(
                        t: Transaction,
                        i: Isin
                ) = EnrichedTransaction(
                        t.id.toString(),
                        LocalDateTime.ofInstant(
                                Instant.ofEpochMilli(t.messageCreation),
                                TimeZone.getDefault().toZoneId()
                        ),
                        t.price,
                        t.currency.toString(),
                        t.quantity,
                        t.instrumentID.toString(),
                        LocalDateTime.ofInstant(
                                Instant.ofEpochMilli(t.bookingDateTime),
                                TimeZone.getDefault().toZoneId()
                        ),
                        LocalDateTime.ofInstant(
                                Instant.ofEpochMilli(t.settlementDateTime),
                                TimeZone.getDefault().toZoneId()
                        ),
                        t.bookingCenterID.toString(),
                        i.isin.toString(),
                        i.name.toString(),
                        i.wkn.toString(),
                        i.symbol.toString(),
                        i.market.toString()
                )
        }
}