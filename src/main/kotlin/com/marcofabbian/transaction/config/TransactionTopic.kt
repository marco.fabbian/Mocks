package com.marcofabbian.transaction.config

import org.springframework.context.annotation.Configuration
import org.springframework.beans.factory.annotation.Value

@Configuration
class TransactionTopic (){

    @Value("\${transaction.topic}")
    lateinit var topic:String

    @Value("\${transaction.replications}")
    lateinit var replications:String

    @Value("\${transaction.partitions}")
    lateinit var partitions:String
}