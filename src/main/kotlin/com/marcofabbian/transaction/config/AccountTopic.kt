package com.marcofabbian.transaction.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class AccountTopic {
    @Value("\${account.topic}")
    lateinit var topic:String

    @Value("\${account.replications}")
    lateinit var replications:String

    @Value("\${account.partitions}")
    lateinit var partitions:String
}