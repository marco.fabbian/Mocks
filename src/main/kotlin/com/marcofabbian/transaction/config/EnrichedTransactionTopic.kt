package com.marcofabbian.transaction.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class EnrichedTransactionTopic {
    @Value("\${enrichedTransaction.topic}")
    lateinit var topic:String

    @Value("\${enrichedTransaction.replications}")
    lateinit var replications:String

    @Value("\${enrichedTransaction.partitions}")
    lateinit var partitions:String
}