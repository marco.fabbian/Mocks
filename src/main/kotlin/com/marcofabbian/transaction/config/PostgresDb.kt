package com.marcofabbian.transaction.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class PostgresDb {
    @Value("\${postgres.host}")
    lateinit var host:String

    @Value("\${postgres.port}")
    lateinit var port:String

    @Value("\${postgres.database}")
    lateinit var database:String

    @Value("\${postgres.username}")
    lateinit var username:String

    @Value("\${postgres.password}")
    lateinit var password:String
}