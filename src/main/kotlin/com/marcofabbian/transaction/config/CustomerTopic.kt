package com.marcofabbian.transaction.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class CustomerTopic {
    @Value("\${customer.topic}")
    lateinit var topic:String

    @Value("\${customer.replications}")
    lateinit var replications:String

    @Value("\${customer.partitions}")
    lateinit var partitions:String
}