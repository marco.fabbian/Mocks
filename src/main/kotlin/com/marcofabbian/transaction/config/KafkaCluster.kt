package com.marcofabbian.transaction.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class KafkaCluster {

    @Value("\${spring.kafka.bootstrap-servers}")
    lateinit var bootstrapServers:String

    @Value("\${spring.kafka.schema-registry}")
    lateinit var schemaRegistry:String
}