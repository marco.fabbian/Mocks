package com.marcofabbian.transaction.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class IsinTopic {
    @Value("\${isin.topic}")
    lateinit var topic:String

    @Value("\${isin.replications}")
    lateinit var replications:String

    @Value("\${isin.partitions}")
    lateinit var partitions:String
}