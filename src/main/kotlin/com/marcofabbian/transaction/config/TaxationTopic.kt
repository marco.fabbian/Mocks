package com.marcofabbian.transaction.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class TaxationTopic {
    @Value("\${taxation.topic}")
    lateinit var topic:String

    @Value("\${taxation.replications}")
    lateinit var replications:String

    @Value("\${taxation.partitions}")
    lateinit var partitions:String
}