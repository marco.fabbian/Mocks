package com.marcofabbian.transaction.stream

import com.marcofabbian.transaction.avroschema.Isin
import com.marcofabbian.transaction.avroschema.Transaction
import com.marcofabbian.transaction.data.EnrichedTransaction
import com.marcofabbian.transaction.client.StreamClient
import com.marcofabbian.transaction.config.IsinTopic
import com.marcofabbian.transaction.config.EnrichedTransactionTopic
import com.marcofabbian.transaction.config.TransactionTopic
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.streams.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.kafka.config.TopicBuilder
import org.springframework.stereotype.Service
import java.util.*

@Service
class TransactionStream(
    val transactionConfig: TransactionTopic,
    val isinConfig: IsinTopic,
    val resultConfig: EnrichedTransactionTopic,
    val streamClient: StreamClient
) {

    private val logger = LoggerFactory.getLogger(TransactionStream::class.java)

    @Value("\${spring.kafka.bootstrap-servers}")
    private lateinit var bootstrapServers:String

    @Value("\${spring.kafka.schema-registry}")
    private lateinit var schemaRegistry:String

    fun transactionStreamTopicSetup(): NewTopic {
        return TopicBuilder
            .name(resultConfig.topic)
            .partitions(resultConfig.partitions.toInt())
            .replicas(resultConfig.replications.toInt())
            .build()
    }


    //@Bean
    fun joinStream(): KafkaStreams {
        return streamClient.stream<Transaction,Isin, EnrichedTransaction>(
            transactionConfig.topic,
            isinConfig.topic,
            resultConfig.topic)
            { t, i -> EnrichedTransaction.mapper(t, i)}
    }
}