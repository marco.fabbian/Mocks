package com.marcofabbian.transaction.jpa

import com.marcofabbian.transaction.data.Taxation
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.util.UUID
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.Transactional

@Service
class TaxationRepository(
    @PersistenceContext
    private val entityManager: EntityManager
) : ITaxationRepository {
    @Transactional
    override fun save(entity: Taxation): Boolean {
        return (entityManager.persist(entity) != null)
    }
}

@Repository
interface ITaxationJPARepository : JpaRepository<Taxation, UUID>, ITaxationRepository {

}

interface ITaxationRepository {
    fun save(entity: Taxation):Boolean
}