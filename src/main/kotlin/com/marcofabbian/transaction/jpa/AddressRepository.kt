package com.marcofabbian.transaction.jpa

import com.marcofabbian.transaction.data.Address
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Service
class AddressRepository(
    @PersistenceContext
    private val entityManager: EntityManager
) : IAddressRepository {

    @Transactional
    override fun save(entity: Address): Boolean {
        return entityManager.persist(entity)!= null
    }
}

@Repository
interface IAddressJPARepository : JpaRepository<Address, UUID>, IAddressRepository {

}

interface IAddressRepository {
    fun save(entity: Address):Boolean
}