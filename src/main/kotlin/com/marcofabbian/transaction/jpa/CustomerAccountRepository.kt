package com.marcofabbian.transaction.jpa

import com.marcofabbian.transaction.data.CustomerAccount
import org.springframework.context.annotation.Bean
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.TypedQuery

@Service
open class CustomerAccountRepository (
    @PersistenceContext
    private val entityManager: EntityManager
) : ICustomerAccountRepository {
    @Transactional
    override fun save(entity: CustomerAccount): Boolean {
        return entityManager.persist(entity)!= null
    }
}

@Repository
interface ICustomAccountJPARepository : JpaRepository<CustomerAccount, UUID>, ICustomerAccountRepository {
}

interface ICustomerAccountRepository {
    fun save(entity:CustomerAccount):Boolean
}