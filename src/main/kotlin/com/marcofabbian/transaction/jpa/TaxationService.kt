package com.marcofabbian.transaction.jpa

import com.marcofabbian.transaction.data.Taxation
import org.springframework.stereotype.Component

@Component
class TaxationService(
    private val repo:TaxationRepository
) {
    fun insert(entity: Taxation):Boolean {
        val result = repo.save(entity)
        return (result != null)
    }
}