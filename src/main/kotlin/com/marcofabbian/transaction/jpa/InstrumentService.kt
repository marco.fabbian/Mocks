package com.marcofabbian.transaction.jpa

import com.marcofabbian.transaction.data.CustomerInformation
import com.marcofabbian.transaction.data.Instrument
import org.springframework.stereotype.Component

@Component
class InstrumentService(
    private val repo:InstrumentRepository
) {
    fun insert(entity: Instrument):Boolean {
        val result = repo.save(entity)
        return (result != null)
    }
}