package com.marcofabbian.transaction.jpa

import com.marcofabbian.transaction.data.CustomerInformation
import com.marcofabbian.transaction.data.Instrument
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Service
class InstrumentRepository(
    @PersistenceContext
    private val entityManager: EntityManager
) : IInstrumentRepository {
    @Transactional
    override fun save(entity: Instrument): Boolean {
        return (entityManager.persist(entity) != null)
    }
}

@Repository
interface IInstrumentJPARepository : JpaRepository<Instrument, UUID>, IInstrumentRepository {

}

interface IInstrumentRepository {
    fun save(entity: Instrument):Boolean
}