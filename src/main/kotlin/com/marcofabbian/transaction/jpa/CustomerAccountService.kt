package com.marcofabbian.transaction.jpa

import com.marcofabbian.transaction.data.CustomerAccount
import org.springframework.stereotype.Component

@Component
class CustomerAccountService(
    private val repo:CustomerAccountRepository
    ){

    fun insert(entity: CustomerAccount):Boolean {
        val result = repo.save(entity)
        return (result != null)
    }
}