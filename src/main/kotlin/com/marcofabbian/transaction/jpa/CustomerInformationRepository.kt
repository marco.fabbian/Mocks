package com.marcofabbian.transaction.jpa

import com.marcofabbian.transaction.data.CustomerAccount
import com.marcofabbian.transaction.data.CustomerInformation
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Service
open class CustomerInformationRepository (
    @PersistenceContext
    private val entityManager: EntityManager
) : ICustomerInformationRepository {
    @Transactional
    override fun save(entity: CustomerInformation): Boolean {
        return (entityManager.persist(entity) != null)
    }
}

@Repository
interface ICustomerInformationJPARepository : JpaRepository<CustomerInformation, UUID>, ICustomerInformationRepository {
}

interface ICustomerInformationRepository {
    fun save(entity: CustomerInformation):Boolean
}