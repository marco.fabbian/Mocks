package com.marcofabbian.transaction.jpa

import com.marcofabbian.transaction.data.CustomerInformation
import org.springframework.stereotype.Component

@Component
class CustomerInformationService(
    private val repo:CustomerInformationRepository
) {
    fun insert(entity: CustomerInformation):Boolean {
        val result = repo.save(entity)
        return (result != null)
    }

}