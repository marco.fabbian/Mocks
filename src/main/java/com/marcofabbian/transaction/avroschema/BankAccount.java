/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package com.marcofabbian.transaction.avroschema;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class BankAccount extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -7565437397135204712L;


  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"BankAccount\",\"namespace\":\"com.marcofabbian.transaction.avroschema\",\"fields\":[{\"name\":\"id\",\"type\":\"string\",\"doc\":\"Account unique identifier\"},{\"name\":\"currency\",\"type\":\"string\",\"doc\":\"Account currency\"},{\"name\":\"type\",\"type\":{\"type\":\"enum\",\"name\":\"BankAccountType\",\"symbols\":[\"CASH\",\"CUSTODY\"]},\"doc\":\"Account type : Cash account, custody account and block chain wallet\"},{\"name\":\"customerId\",\"type\":\"string\",\"doc\":\"Customer id\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static final SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<BankAccount> ENCODER =
      new BinaryMessageEncoder<>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<BankAccount> DECODER =
      new BinaryMessageDecoder<>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<BankAccount> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<BankAccount> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<BankAccount> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this BankAccount to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a BankAccount from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a BankAccount instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static BankAccount fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  /** Account unique identifier */
  private java.lang.CharSequence id;
  /** Account currency */
  private java.lang.CharSequence currency;
  /** Account type : Cash account, custody account and block chain wallet */
  private com.marcofabbian.transaction.avroschema.BankAccountType type;
  /** Customer id */
  private java.lang.CharSequence customerId;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public BankAccount() {}

  /**
   * All-args constructor.
   * @param id Account unique identifier
   * @param currency Account currency
   * @param type Account type : Cash account, custody account and block chain wallet
   * @param customerId Customer id
   */
  public BankAccount(java.lang.CharSequence id, java.lang.CharSequence currency, com.marcofabbian.transaction.avroschema.BankAccountType type, java.lang.CharSequence customerId) {
    this.id = id;
    this.currency = currency;
    this.type = type;
    this.customerId = customerId;
  }

  @Override
  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }

  @Override
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }

  // Used by DatumWriter.  Applications should not call.
  @Override
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return id;
    case 1: return currency;
    case 2: return type;
    case 3: return customerId;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  // Used by DatumReader.  Applications should not call.
  @Override
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: id = (java.lang.CharSequence)value$; break;
    case 1: currency = (java.lang.CharSequence)value$; break;
    case 2: type = (com.marcofabbian.transaction.avroschema.BankAccountType)value$; break;
    case 3: customerId = (java.lang.CharSequence)value$; break;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  /**
   * Gets the value of the 'id' field.
   * @return Account unique identifier
   */
  public java.lang.CharSequence getId() {
    return id;
  }


  /**
   * Sets the value of the 'id' field.
   * Account unique identifier
   * @param value the value to set.
   */
  public void setId(java.lang.CharSequence value) {
    this.id = value;
  }

  /**
   * Gets the value of the 'currency' field.
   * @return Account currency
   */
  public java.lang.CharSequence getCurrency() {
    return currency;
  }


  /**
   * Sets the value of the 'currency' field.
   * Account currency
   * @param value the value to set.
   */
  public void setCurrency(java.lang.CharSequence value) {
    this.currency = value;
  }

  /**
   * Gets the value of the 'type' field.
   * @return Account type : Cash account, custody account and block chain wallet
   */
  public com.marcofabbian.transaction.avroschema.BankAccountType getType() {
    return type;
  }


  /**
   * Sets the value of the 'type' field.
   * Account type : Cash account, custody account and block chain wallet
   * @param value the value to set.
   */
  public void setType(com.marcofabbian.transaction.avroschema.BankAccountType value) {
    this.type = value;
  }

  /**
   * Gets the value of the 'customerId' field.
   * @return Customer id
   */
  public java.lang.CharSequence getCustomerId() {
    return customerId;
  }


  /**
   * Sets the value of the 'customerId' field.
   * Customer id
   * @param value the value to set.
   */
  public void setCustomerId(java.lang.CharSequence value) {
    this.customerId = value;
  }

  /**
   * Creates a new BankAccount RecordBuilder.
   * @return A new BankAccount RecordBuilder
   */
  public static com.marcofabbian.transaction.avroschema.BankAccount.Builder newBuilder() {
    return new com.marcofabbian.transaction.avroschema.BankAccount.Builder();
  }

  /**
   * Creates a new BankAccount RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new BankAccount RecordBuilder
   */
  public static com.marcofabbian.transaction.avroschema.BankAccount.Builder newBuilder(com.marcofabbian.transaction.avroschema.BankAccount.Builder other) {
    if (other == null) {
      return new com.marcofabbian.transaction.avroschema.BankAccount.Builder();
    } else {
      return new com.marcofabbian.transaction.avroschema.BankAccount.Builder(other);
    }
  }

  /**
   * Creates a new BankAccount RecordBuilder by copying an existing BankAccount instance.
   * @param other The existing instance to copy.
   * @return A new BankAccount RecordBuilder
   */
  public static com.marcofabbian.transaction.avroschema.BankAccount.Builder newBuilder(com.marcofabbian.transaction.avroschema.BankAccount other) {
    if (other == null) {
      return new com.marcofabbian.transaction.avroschema.BankAccount.Builder();
    } else {
      return new com.marcofabbian.transaction.avroschema.BankAccount.Builder(other);
    }
  }

  /**
   * RecordBuilder for BankAccount instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<BankAccount>
    implements org.apache.avro.data.RecordBuilder<BankAccount> {

    /** Account unique identifier */
    private java.lang.CharSequence id;
    /** Account currency */
    private java.lang.CharSequence currency;
    /** Account type : Cash account, custody account and block chain wallet */
    private com.marcofabbian.transaction.avroschema.BankAccountType type;
    /** Customer id */
    private java.lang.CharSequence customerId;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$, MODEL$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(com.marcofabbian.transaction.avroschema.BankAccount.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.id)) {
        this.id = data().deepCopy(fields()[0].schema(), other.id);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
      if (isValidValue(fields()[1], other.currency)) {
        this.currency = data().deepCopy(fields()[1].schema(), other.currency);
        fieldSetFlags()[1] = other.fieldSetFlags()[1];
      }
      if (isValidValue(fields()[2], other.type)) {
        this.type = data().deepCopy(fields()[2].schema(), other.type);
        fieldSetFlags()[2] = other.fieldSetFlags()[2];
      }
      if (isValidValue(fields()[3], other.customerId)) {
        this.customerId = data().deepCopy(fields()[3].schema(), other.customerId);
        fieldSetFlags()[3] = other.fieldSetFlags()[3];
      }
    }

    /**
     * Creates a Builder by copying an existing BankAccount instance
     * @param other The existing instance to copy.
     */
    private Builder(com.marcofabbian.transaction.avroschema.BankAccount other) {
      super(SCHEMA$, MODEL$);
      if (isValidValue(fields()[0], other.id)) {
        this.id = data().deepCopy(fields()[0].schema(), other.id);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.currency)) {
        this.currency = data().deepCopy(fields()[1].schema(), other.currency);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.type)) {
        this.type = data().deepCopy(fields()[2].schema(), other.type);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.customerId)) {
        this.customerId = data().deepCopy(fields()[3].schema(), other.customerId);
        fieldSetFlags()[3] = true;
      }
    }

    /**
      * Gets the value of the 'id' field.
      * Account unique identifier
      * @return The value.
      */
    public java.lang.CharSequence getId() {
      return id;
    }


    /**
      * Sets the value of the 'id' field.
      * Account unique identifier
      * @param value The value of 'id'.
      * @return This builder.
      */
    public com.marcofabbian.transaction.avroschema.BankAccount.Builder setId(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.id = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'id' field has been set.
      * Account unique identifier
      * @return True if the 'id' field has been set, false otherwise.
      */
    public boolean hasId() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'id' field.
      * Account unique identifier
      * @return This builder.
      */
    public com.marcofabbian.transaction.avroschema.BankAccount.Builder clearId() {
      id = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'currency' field.
      * Account currency
      * @return The value.
      */
    public java.lang.CharSequence getCurrency() {
      return currency;
    }


    /**
      * Sets the value of the 'currency' field.
      * Account currency
      * @param value The value of 'currency'.
      * @return This builder.
      */
    public com.marcofabbian.transaction.avroschema.BankAccount.Builder setCurrency(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.currency = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'currency' field has been set.
      * Account currency
      * @return True if the 'currency' field has been set, false otherwise.
      */
    public boolean hasCurrency() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'currency' field.
      * Account currency
      * @return This builder.
      */
    public com.marcofabbian.transaction.avroschema.BankAccount.Builder clearCurrency() {
      currency = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'type' field.
      * Account type : Cash account, custody account and block chain wallet
      * @return The value.
      */
    public com.marcofabbian.transaction.avroschema.BankAccountType getType() {
      return type;
    }


    /**
      * Sets the value of the 'type' field.
      * Account type : Cash account, custody account and block chain wallet
      * @param value The value of 'type'.
      * @return This builder.
      */
    public com.marcofabbian.transaction.avroschema.BankAccount.Builder setType(com.marcofabbian.transaction.avroschema.BankAccountType value) {
      validate(fields()[2], value);
      this.type = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'type' field has been set.
      * Account type : Cash account, custody account and block chain wallet
      * @return True if the 'type' field has been set, false otherwise.
      */
    public boolean hasType() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'type' field.
      * Account type : Cash account, custody account and block chain wallet
      * @return This builder.
      */
    public com.marcofabbian.transaction.avroschema.BankAccount.Builder clearType() {
      type = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'customerId' field.
      * Customer id
      * @return The value.
      */
    public java.lang.CharSequence getCustomerId() {
      return customerId;
    }


    /**
      * Sets the value of the 'customerId' field.
      * Customer id
      * @param value The value of 'customerId'.
      * @return This builder.
      */
    public com.marcofabbian.transaction.avroschema.BankAccount.Builder setCustomerId(java.lang.CharSequence value) {
      validate(fields()[3], value);
      this.customerId = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'customerId' field has been set.
      * Customer id
      * @return True if the 'customerId' field has been set, false otherwise.
      */
    public boolean hasCustomerId() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'customerId' field.
      * Customer id
      * @return This builder.
      */
    public com.marcofabbian.transaction.avroschema.BankAccount.Builder clearCustomerId() {
      customerId = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public BankAccount build() {
      try {
        BankAccount record = new BankAccount();
        record.id = fieldSetFlags()[0] ? this.id : (java.lang.CharSequence) defaultValue(fields()[0]);
        record.currency = fieldSetFlags()[1] ? this.currency : (java.lang.CharSequence) defaultValue(fields()[1]);
        record.type = fieldSetFlags()[2] ? this.type : (com.marcofabbian.transaction.avroschema.BankAccountType) defaultValue(fields()[2]);
        record.customerId = fieldSetFlags()[3] ? this.customerId : (java.lang.CharSequence) defaultValue(fields()[3]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<BankAccount>
    WRITER$ = (org.apache.avro.io.DatumWriter<BankAccount>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<BankAccount>
    READER$ = (org.apache.avro.io.DatumReader<BankAccount>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    out.writeString(this.id);

    out.writeString(this.currency);

    out.writeEnum(this.type.ordinal());

    out.writeString(this.customerId);

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      this.id = in.readString(this.id instanceof Utf8 ? (Utf8)this.id : null);

      this.currency = in.readString(this.currency instanceof Utf8 ? (Utf8)this.currency : null);

      this.type = com.marcofabbian.transaction.avroschema.BankAccountType.values()[in.readEnum()];

      this.customerId = in.readString(this.customerId instanceof Utf8 ? (Utf8)this.customerId : null);

    } else {
      for (int i = 0; i < 4; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          this.id = in.readString(this.id instanceof Utf8 ? (Utf8)this.id : null);
          break;

        case 1:
          this.currency = in.readString(this.currency instanceof Utf8 ? (Utf8)this.currency : null);
          break;

        case 2:
          this.type = com.marcofabbian.transaction.avroschema.BankAccountType.values()[in.readEnum()];
          break;

        case 3:
          this.customerId = in.readString(this.customerId instanceof Utf8 ? (Utf8)this.customerId : null);
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










