/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package com.marcofabbian.transaction.avroschema;
@org.apache.avro.specific.AvroGenerated
public enum BankAccountType implements org.apache.avro.generic.GenericEnumSymbol<BankAccountType> {
  CASH, CUSTODY  ;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"enum\",\"name\":\"BankAccountType\",\"namespace\":\"com.marcofabbian.transaction.avroschema\",\"symbols\":[\"CASH\",\"CUSTODY\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  @Override
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
}
