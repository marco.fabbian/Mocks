import org.jetbrains.kotlin.gradle.tasks.*
import java.util.regex.Pattern.compile
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


var javaVersion = JavaVersion.VERSION_11
group = "com.marcofabbian"
version = "0.0.1-SNAPSHOT"


plugins {
	java

	id("org.springframework.boot") version "2.7.9"
	id("io.spring.dependency-management") version "1.0.13.RELEASE"

	kotlin("jvm") version "1.6.21"
	kotlin("plugin.spring") version "1.6.21"
	kotlin("plugin.jpa") version "1.3.31"
	kotlin("plugin.serialization") version "1.4.21" // <-- (1)
}





repositories {
	google()
	gradlePluginPortal()
	mavenCentral()
	jcenter()
	maven { url = uri("https://packages.confluent.io/maven/") }
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = javaVersion.toString()
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}


java {
	sourceCompatibility = javaVersion
	targetCompatibility = javaVersion
}


dependencies {
	implementation("org.apache.avro:avro:1.10.1")
	implementation("org.springframework.boot:spring-boot-starter")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

	// Kafkja dependencies
	implementation("org.apache.kafka:kafka-clients:3.2.1")
	implementation("org.apache.kafka:kafka_2.13:3.2.1")
	implementation("org.apache.kafka:kafka-streams:3.2.1")

	//Postgres database dependencies
	implementation("com.github.jasync-sql:jasync-postgresql:2.1.16")


	//Springframework dependencies
	implementation("org.springframework.kafka:spring-kafka:2.9.0")

	//Avro dependencies
	implementation("org.apache.avro:avro:1.11.0")
	implementation("io.confluent:kafka-avro-serializer:5.3.0")
	implementation("io.confluent:kafka-streams-avro-serde:7.2.1")
	implementation("org.apache.commons:commons-compress:1.21")
	compile ("com.commercehub.gradle.plugin.avro:gradle-avro-plugin:0.22.0")

	//Hibernate
	compile("org.hibernate:hibernate-core:5.2.15.Final")
	implementation("org.springframework.data:spring-data-jpa:2.3.9.RELEASE")
	implementation("org.springframework.data:spring-data-commons:2.2.0.RELEASE")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.6.7")
	implementation("org.eclipse.persistence:javax.persistence:2.2.1")
	implementation("org.postgresql:postgresql")

	//Rest api
	implementation("org.springframework.boot:spring-boot-starter-hateoas:2.6.0")

	//Json library
	implementation("com.google.code.gson:gson:2.8.5")

	compile("org.javassist:javassist:3.23.1-GA")


	//Test
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation(kotlin("test"))

	//Hibernate
	testImplementation("org.hibernate:hibernate-testing:5.2.15.Final")
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    languageVersion = "1.7"
}